
package tcpserver;
import java.util.Vector;


public class FCFS {
    
    
    protected int[] arrivalTime;
    protected int[] burstTime;
    protected int[] job;
    protected int[] jobIdle;
    protected int numberOfProcess;
    protected int[] waitingTime;
    protected int[] finishedTime;
    protected int averageWT,averageTTsum;
    protected int jobs;

    public FCFS (int[] aT,int[] bT,int[] job,int num)
    {
        arrivalTime = aT;
        burstTime = bT;
        this.job = job;
        numberOfProcess = num;
        waitingTime = new int[numberOfProcess];
        finishedTime = new int[numberOfProcess];
        jobs = 0;
    }

    FCFS() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

public void FCFS()
{
    int firstCome,tempArrivalTime,tempBurst;
 //sort processes 
    for (int i = 0; i < (numberOfProcess - 1); i++) 
    {
   for (int j = (i + 1); j < numberOfProcess; j++)
   {
        if (arrivalTime[j] < arrivalTime[i]) 
        {

            firstCome = job[j];
            job[j] = job[i];
            job[i] = firstCome;
            tempArrivalTime = arrivalTime[j];
            arrivalTime[j] = arrivalTime[i];
            arrivalTime[i] = tempArrivalTime;
            tempBurst = burstTime[j];
            burstTime[j] = burstTime[i];
            burstTime[i] = tempBurst;

                }

        }

    }

    System.out.println("\n==Displaying Table of Jobs Sorted According to Arrival Time==");
    displaySorted();  
    System.out.println("======DISPLAYING GANTT CHART======");
    solveWaitingTime();
    solveAverageTime();


}
public void solveAverageTime()
{
    //ATT
    for(int i = 0;i<numberOfProcess;i++)
    averageTTsum = averageTTsum+(finishedTime[i]-arrivalTime[i]);
    //AWT
    for(int j=0;j<numberOfProcess;j++)
        averageWT = averageWT+(waitingTime[j] - arrivalTime[j]);


    double aTT = (float)averageTTsum/numberOfProcess;
    double aWT=(float)averageWT/numberOfProcess;
    System.out.format("ATT: %.2f ",aTT);
    System.out.println("");
    System.out.format("AWT: %.2f ",aWT);
}

public void solveWaitingTime()
{   int ctr=0;
    Vector<Integer> idleWT = new Vector<Integer>();
    Vector<Boolean> idle = new Vector<Boolean>();
    for(int z = 0; z < numberOfProcess; z++) 
    {
        if(ctr > arrivalTime[z])                        
        {idle.add(Boolean.TRUE);                          
            for(int k = 0; k < burstTime[z]; k++)       
            {

            ctr++;                              

                }
            jobs++;

        } 
        else                                        
        {
            while(ctr <= arrivalTime[z]) 
            {
                if(ctr == arrivalTime[z])                   
                {
                   jobs++;                                  
                    for(int j = arrivalTime[z]; j < (arrivalTime[z] + burstTime[z]); j++)
                        {                                                               
                            ctr++;

                        }
                    idle.add(Boolean.TRUE);             
                } 
                else                                    
                {
                    jobs++;             
                ctr++;                                     
                    idle.add(Boolean.FALSE);                
                }

            }

            }

        finishedTime[z] = ctr;                 
        if(z==0)                                
       idleWT.add(0);                          
        else idleWT.add(ctr);                   
}
     waitingTime[0] = 0;
    for(int z = 1;z<numberOfProcess;z++)
    {  
        waitingTime[z] = finishedTime[z] - burstTime[z];

    }

   System.out.println(""+idleWT.toString());               
      System.out.println(""+idle.toString());  
       System.out.println("Jobs: "+jobs);   
       int ctr2 = 0;

    for(int y = 0; y < numberOfProcess; y++)        
        {
            if(idle.elementAt(ctr2)==false)                     
            {   if(ctr2==0) 
            {System.out.print("|I"+(waitingTime[y+1])+" |"); ctr2++;}   
            else {
                System.out.print("|I "+(idleWT.elementAt(y)-waitingTime[y])+" |");
                ctr2++;
            }

            }
            System.out.print("|P"+job[y]+"("+burstTime[y]+")"+" |");            
            ctr2++;
        }   
    System.out.println("");

    for(int x = 0;x<numberOfProcess;x++)
    {  if(idleWT.elementAt(x) == 0)
        System.out.print(""+waitingTime[x]);
    else System.out.print("      "+waitingTime[x]+ "      "+idleWT.elementAt(x));
    }
    System.out.println("");
}

public void displaySorted()
{
    System.out.println("\n------------------------------------");

  System.out.println("Process | Arrival Time | CPU Burst ");


    for(int y = 0; y < numberOfProcess; y++) 
        {
            System.out.println("P" + job[y] + "\t\t"
            + arrivalTime[y] +"\t      " 
            + burstTime[y]);

        }

  System.out.println("------------------------------------\n");
}
}



    
    
    
    
    
    
    
    
    
    
    
    
    



      

    