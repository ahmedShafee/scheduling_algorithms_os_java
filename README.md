# Scheduling_Algorithms_OS_Java
Three Scheduling algorithms that are used by OSs are written in Java Language.
The first Algorithm is the **_first-come-first-served (FCFS)_** algorithm that is used by OS to schedule the processes to be run according to the priority of their arrival time.
The second scheduling is the **_shortest-Job-First (SJF)_ ** that schedules the process so that the job with the shortest execution time is executed first.
The third scheduling algorithm is the **_Round Robin (RR)_** that is used the time sharing concept to schedule the processes.
The three schedulers are written in  Java language using Netbeans and Eclips frameworks.
